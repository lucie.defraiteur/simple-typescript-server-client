define("personne", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Personne = void 0;
    var Personne = /** @class */ (function () {
        function Personne() {
        }
        Personne.prototype.DisBonjour = function () {
            console.log("bonjour");
        };
        return Personne;
    }());
    exports.Personne = Personne;
});
define("main", ["require", "exports", "personne"], function (require, exports, personne_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var elems = document.querySelectorAll(".interupteur_texte");
    var _loop_1 = function () {
        var elem = elems[i];
        var txtElem = elem.parentElement.querySelector(".texte");
        elem.addEventListener("click", function (evt) {
            var oldVis = txtElem.style.display;
            var wasHidden = (oldVis == "none");
            txtElem.style.display = wasHidden ? "block" : "none";
            elem.innerHTML = wasHidden ? 'Cacher le texte.' : "Afficher le texte.";
        });
        txtElem.style.display = "none"; // wasHidden ? "visible" : "hidden";
        elem.innerHTML = "Afficher le texte.";
    };
    for (var i = 0; i < elems.length; i++) {
        _loop_1();
    }
    //
    var personne = new personne_1.Personne();
    personne.DisBonjour();
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVpbGQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvcGVyc29ubmUudHMiLCIuLi9zcmMvbWFpbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0lBQUE7UUFBQTtRQU1BLENBQUM7UUFKQSw2QkFBVSxHQUFWO1lBRUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN4QixDQUFDO1FBQ0YsZUFBQztJQUFELENBQUMsQUFORCxJQU1DO0lBTlksNEJBQVE7Ozs7O0lDRXJCLElBQUksS0FBSyxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDOztRQUV4RCxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFtQixDQUFDO1FBRTNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsVUFBQyxHQUFHO1lBRS9CLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1lBQ25DLElBQUksU0FBUyxHQUFHLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxDQUFDO1lBQ25DLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFFckQsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQztRQUMzRSxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxDQUFBLG9DQUFvQztRQUNuRSxJQUFJLENBQUMsU0FBUyxHQUFHLG9CQUFvQixDQUFDOztJQWIxQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUU7O0tBY3BDO0lBQ0QsRUFBRTtJQUNGLElBQUksUUFBUSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO0lBQzlCLFFBQVEsQ0FBQyxVQUFVLEVBQUUsQ0FBQyJ9