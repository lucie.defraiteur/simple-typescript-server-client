"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http = require('http');
var url = require('url');
var fs = require('fs');
var path = require('path');
var exec = require("child_process").exec;
var port = process.argv[2] || 9000;
var child_process_1 = require("child_process");
process.chdir('build/');
http.createServer(function (req, res) {
    console.log(req.method + " " + req.url);
    // parse URL
    var parsedUrl = url.parse(req.url);
    // extract URL path
    var pathname = "." + parsedUrl.pathname;
    console.log("pathName: " + pathname);
    if (pathname == "./") {
        pathname = "./index.html";
    }
    // based on the URL path, extract the file extention. e.g. .js, .doc, ...
    var ext = path.parse(pathname).ext;
    // maps file extention to MIME typere
    var map = {
        '.ico': 'image/x-icon',
        '.html': 'text/html',
        '.js': 'text/javascript',
        '.json': 'application/json',
        '.css': 'text/css',
        '.png': 'image/png',
        '.jpg': 'image/jpeg',
        '.wav': 'audio/wav',
        '.mp3': 'audio/mpeg',
        '.svg': 'image/svg+xml',
        '.pdf': 'application/pdf',
        '.doc': 'application/msword'
    };
    fs.exists(pathname, function (exist) {
        if (!exist) {
            // if the file is not found, return 404
            res.statusCode = 404;
            res.end("File " + pathname + " not found!");
            return;
        }
        // if is a directory search for index file matching the extention
        if (fs.statSync(pathname).isDirectory())
            pathname += '/index' + ext;
        // read file from file system
        fs.readFile(pathname, function (err, data) {
            if (err) {
                res.statusCode = 500;
                res.end("Error getting the file: " + err + ".");
            }
            else {
                // if the file is found, set Content-type and send data
                res.setHeader('Content-type', map[ext] || 'text/plain');
                res.end(data);
            }
        });
    });
}).listen(parseInt(port));
console.log("Server listening on http://localhost:" + port);
var options = { shell: true, cwd: "../" };
var ls = child_process_1.spawn("tsc", ["-w", "--p", "./"], options);
ls.stdout.on("data", function (data) {
    console.log("stdout: " + data);
});
ls.stderr.on("data", function (data) {
    console.log("stderr: " + data);
});
ls.on('error', function (error) {
    console.log("error: " + error.message);
});
ls.on("close", function (code) {
    console.log("child process exited with code " + code);
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsSUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQzdCLElBQU0sR0FBRyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUMzQixJQUFNLEVBQUUsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDekIsSUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3JCLElBQUEsSUFBSSxHQUFLLE9BQU8sQ0FBQyxlQUFlLENBQUMsS0FBN0IsQ0FBOEI7QUFDMUMsSUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUM7QUFDckMsK0NBQWtEO0FBQ2xELE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7QUFFeEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEdBQUcsRUFBRSxHQUFHO0lBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUksR0FBRyxDQUFDLE1BQU0sU0FBSSxHQUFHLENBQUMsR0FBSyxDQUFDLENBQUM7SUFFeEMsWUFBWTtJQUNaLElBQU0sU0FBUyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3JDLG1CQUFtQjtJQUNuQixJQUFJLFFBQVEsR0FBRyxNQUFJLFNBQVMsQ0FBQyxRQUFVLENBQUM7SUFFeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLENBQUM7SUFDckMsSUFBSSxRQUFRLElBQUksSUFBSSxFQUNwQjtRQUNJLFFBQVEsR0FBRyxjQUFjLENBQUM7S0FDN0I7SUFDRCx5RUFBeUU7SUFDekUsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFDckMscUNBQXFDO0lBQ3JDLElBQU0sR0FBRyxHQUFHO1FBQ1YsTUFBTSxFQUFFLGNBQWM7UUFDdEIsT0FBTyxFQUFFLFdBQVc7UUFDcEIsS0FBSyxFQUFFLGlCQUFpQjtRQUN4QixPQUFPLEVBQUUsa0JBQWtCO1FBQzNCLE1BQU0sRUFBRSxVQUFVO1FBQ2xCLE1BQU0sRUFBRSxXQUFXO1FBQ25CLE1BQU0sRUFBRSxZQUFZO1FBQ3BCLE1BQU0sRUFBRSxXQUFXO1FBQ25CLE1BQU0sRUFBRSxZQUFZO1FBQ3BCLE1BQU0sRUFBRSxlQUFlO1FBQ3ZCLE1BQU0sRUFBRSxpQkFBaUI7UUFDekIsTUFBTSxFQUFFLG9CQUFvQjtLQUM3QixDQUFDO0lBRUYsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsVUFBVSxLQUFLO1FBQ2pDLElBQUcsQ0FBQyxLQUFLLEVBQUU7WUFDVCx1Q0FBdUM7WUFDdkMsR0FBRyxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUM7WUFDckIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFRLFFBQVEsZ0JBQWEsQ0FBQyxDQUFDO1lBQ3ZDLE9BQU87U0FDUjtRQUVELGlFQUFpRTtRQUNqRSxJQUFJLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxFQUFFO1lBQUUsUUFBUSxJQUFJLFFBQVEsR0FBRyxHQUFHLENBQUM7UUFFcEUsNkJBQTZCO1FBQzdCLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLFVBQVMsR0FBRyxFQUFFLElBQUk7WUFDdEMsSUFBRyxHQUFHLEVBQUM7Z0JBQ0wsR0FBRyxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUM7Z0JBQ3JCLEdBQUcsQ0FBQyxHQUFHLENBQUMsNkJBQTJCLEdBQUcsTUFBRyxDQUFDLENBQUM7YUFDNUM7aUJBQU07Z0JBQ0wsdURBQXVEO2dCQUN2RCxHQUFHLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksWUFBWSxDQUFFLENBQUM7Z0JBQ3pELEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDZjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7QUFHTCxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQVcsQ0FBQyxDQUFDLENBQUM7QUFFakMsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQ0FBd0MsSUFBTSxDQUFDLENBQUM7QUFLNUQsSUFBSSxPQUFPLEdBQWlCLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFDLENBQUM7QUFDdEQsSUFBTSxFQUFFLEdBQUsscUJBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0FBRXhELEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxVQUFBLElBQUk7SUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFXLElBQU0sQ0FBQyxDQUFDO0FBQ25DLENBQUMsQ0FBQyxDQUFDO0FBRUgsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLFVBQUEsSUFBSTtJQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLGFBQVcsSUFBTSxDQUFDLENBQUM7QUFDbkMsQ0FBQyxDQUFDLENBQUM7QUFFSCxFQUFFLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFDLEtBQUs7SUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFVLEtBQUssQ0FBQyxPQUFTLENBQUMsQ0FBQztBQUMzQyxDQUFDLENBQUMsQ0FBQztBQUVILEVBQUUsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQUEsSUFBSTtJQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0NBQWtDLElBQU0sQ0FBQyxDQUFDO0FBQzFELENBQUMsQ0FBQyxDQUFDIn0=