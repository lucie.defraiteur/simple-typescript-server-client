import { Personne } from "./personne";

var elems = document.querySelectorAll(".interupteur_texte");
for (var i = 0; i < elems.length; i++) {
    let elem = elems[i];
    let txtElem = elem.parentElement.querySelector(".texte") as HTMLDivElement;

    elem.addEventListener("click", (evt) => {

        let oldVis = txtElem.style.display;
        let wasHidden = (oldVis == "none");
        txtElem.style.display = wasHidden ? "block" : "none";

        elem.innerHTML = wasHidden ? 'Cacher le texte.' : "Afficher le texte.";
    });
    txtElem.style.display = "none";// wasHidden ? "visible" : "hidden";
    elem.innerHTML = "Afficher le texte.";
}
//
var personne = new Personne();
personne.DisBonjour();